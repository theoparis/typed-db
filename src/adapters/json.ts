import { JsonDB } from "node-json-db";
import { Adapter } from "./index.js";

export interface JsonDBAdapterOptions {
    path: string;
}

export class JsonAdapter extends Adapter<JsonDBAdapterOptions> {
    db: JsonDB;
    constructor(options: { path: string }) {
        super(options);
        this.db = new JsonDB(this.options.path, true, true);
    }

    has(key: string): boolean {
        return this.db.exists(key);
    }

    get<T>(key: string): T {
        return this.db.getData(key);
    }

    set<T>(key: string, value: T, override = false) {
        return this.db.push(key, value, override);
    }

    del(key: string) {
        return this.db.delete(key);
    }

    reload() {
        this.db.reload();
    }
}
