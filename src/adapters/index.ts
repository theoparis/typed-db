export abstract class Adapter<O = unknown> {
    constructor(public readonly options: O) {}

    abstract get<T>(key: string): T;

    abstract set<T>(key: string, value: T, override?: boolean): void;

    abstract del(key: string): void;

    abstract reload(): void;

    abstract has(key: string): boolean;
}
