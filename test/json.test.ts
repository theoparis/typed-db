import { TypedDB, JsonAdapter } from "../src/index.js";
import { ContentDef, Login, Restaurant } from "./content";

const db = new TypedDB<ContentDef>(new JsonAdapter({ path: "data.json" }));

test("single value db set, merge, & get", () => {
    const login: Login = { username: "user", password: "pass" };

    db.set("/login", login);
    db.merge("/login", { username: "user1" });
    const l = db.get("/login");
    expect(l).toBeDefined();
    expect(l.username).toBe("user1");
});
test("array db", () => {
    const restaurant: Restaurant = {
        chef: "hello",
        memberCount: 2,
        name: "foo",
        turnOver: 10
    };
    db.set("/restaurants", [restaurant, restaurant]);
    let rs = db.get("/restaurants");
    expect(rs).toBeDefined();
    expect(rs[0].chef).toBe(restaurant.chef);
    db.push("/restaurants", restaurant);
    rs = db.get("/restaurants");
    expect(rs).toBeDefined();
    expect(rs[2].chef).toBe(restaurant.chef);

    let r = db.get("/restaurants", -1);
    expect(r).toBeDefined();
    expect(r.chef).toBe(restaurant.chef);

    r = db.get("/restaurants", 2);
    expect(r).toBeDefined();
    expect(r.chef).toBe(restaurant.chef);

    db.merge("/restaurants", { name: "12" }, 1);
    r = db.get("/restaurants", 1);
    expect(r).toBeDefined();
    expect(r.name).toBe("12");
});

test("dictionary db", () => {
    db.set("/teams", { test: "coucou" });
    const t = db.get("/teams");
    expect(t).toBeDefined();
    expect(t.test).toBe("coucou");

    db.push("/teams", "mydata", "here");
    expect(db.get("/teams", "here")).toBe("mydata");
    db.merge("/teams", "merged", "here");
    expect(db.get("/teams", "here")).toBe("merged");

    expect(db.exists("/teams", "")).toBeTruthy();
});
